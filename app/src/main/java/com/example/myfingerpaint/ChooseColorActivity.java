package com.example.myfingerpaint;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;

import com.example.myfingerpaint.commonentities.DataTransferClass;

import java.util.Random;

public class ChooseColorActivity extends AppCompatActivity {

    TextView textViewRed, textViewGreen, textViewBlue;
    SeekBar seekBarRed, seekBarGreen, seekBarBlue;

    TextView textViewNewColor;

    Button buttonSaveAndExit, buttonCancel, buttonRandomColor;

    Random random;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_color);

        random = new Random();

        textViewRed = findViewById(R.id.textViewRed);
        seekBarRed = findViewById(R.id.seekBarRed);

        textViewGreen = findViewById(R.id.textViewGreen);
        seekBarGreen = findViewById(R.id.seekBarGreen);

        textViewBlue = findViewById(R.id.textViewBlue);
        seekBarBlue = findViewById(R.id.seekBarBlue);

        textViewNewColor = findViewById(R.id.textViewNewColor);

        buttonSaveAndExit = findViewById(R.id.buttonSaveAndExit);
        buttonCancel = findViewById(R.id.buttonCancel);
        buttonRandomColor = findViewById(R.id.buttonRandomColor);

        seekBarRed.setOnSeekBarChangeListener(seekBarRedOnSeek);
        seekBarGreen.setOnSeekBarChangeListener(seekBarGreenOnSeek);
        seekBarBlue.setOnSeekBarChangeListener(seekBarBlueOnSeek);

        buttonSaveAndExit.setOnClickListener(buttonSaveAndExitOnClick);
        buttonCancel.setOnClickListener(buttonCancelOnClick);
        buttonRandomColor.setOnClickListener(buttonRandomColorOnClick);

    }

    private void ShowResultColor()
    {
        int r = seekBarRed.getProgress();
        int g = seekBarGreen.getProgress();
        int b = seekBarBlue.getProgress();

        textViewNewColor.setBackgroundColor(Color.argb(255,r,g,b));
    }

    SeekBar.OnSeekBarChangeListener seekBarRedOnSeek = new SeekBar.OnSeekBarChangeListener() {
        @Override
        public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
            ShowResultColor();

            textViewRed.setText("Red: "+seekBarRed.getProgress());
        }
        @Override
        public void onStartTrackingTouch(SeekBar seekBar) { }
        @Override
        public void onStopTrackingTouch(SeekBar seekBar) { }
    };

    SeekBar.OnSeekBarChangeListener seekBarGreenOnSeek = new SeekBar.OnSeekBarChangeListener() {
        @Override
        public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
            ShowResultColor();

            textViewGreen.setText("Green: "+seekBarGreen.getProgress());
        }
        @Override
        public void onStartTrackingTouch(SeekBar seekBar) { }
        @Override
        public void onStopTrackingTouch(SeekBar seekBar) { }
    };

    SeekBar.OnSeekBarChangeListener seekBarBlueOnSeek = new SeekBar.OnSeekBarChangeListener() {
        @Override
        public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
            ShowResultColor();

            textViewBlue.setText("Blue: "+seekBarBlue.getProgress());
        }
        @Override
        public void onStartTrackingTouch(SeekBar seekBar) { }
        @Override
        public void onStopTrackingTouch(SeekBar seekBar) { }
    };


    View.OnClickListener buttonCancelOnClick = new View.OnClickListener() {
        @Override
        public void onClick(View view)
        {
            DataTransferClass.LastActivity = DataTransferClass.ActivityType.ChooseColorActivity;
            DataTransferClass.IsCanceled = true;
            finish();
        }
    };

    View.OnClickListener buttonSaveAndExitOnClick = new View.OnClickListener() {
        @Override
        public void onClick(View view)
        {
            DataTransferClass.LastActivity = DataTransferClass.ActivityType.ChooseColorActivity;
            DataTransferClass.IsCanceled = false;

            DataTransferClass.LineColorR = seekBarRed.getProgress();
            DataTransferClass.LineColorG = seekBarGreen.getProgress();
            DataTransferClass.LineColorB = seekBarBlue.getProgress();

            finish();
        }
    };

    View.OnClickListener buttonRandomColorOnClick = new View.OnClickListener() {
        @Override
        public void onClick(View view)
        {
            int r = random.nextInt(255);
            int g = random.nextInt(255);
            int b = random.nextInt(255);

            seekBarRed.setProgress(r);
            seekBarGreen.setProgress(g);
            seekBarBlue.setProgress(b);

            textViewRed.setText("Red: "+seekBarRed.getProgress());
            textViewGreen.setText("Green: "+seekBarGreen.getProgress());
            textViewBlue.setText("Blue: "+seekBarBlue.getProgress());

            ShowResultColor();
        }
    };


}
